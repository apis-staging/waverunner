# When the module is loaded R_LIBS_USER is set to ~/R/X.X. 
# It does respect R_LIBS_USER set in e.g. .bash_profile, but puts it last
r_libs_user <- strsplit(Sys.getenv('R_LIBS_USER'), ':')[[1]]

# We need to basically re-order the .libPaths, since system libraries are put
# first for some reason
fixed_libpaths <- c(r_libs_user[length(r_libs_user)],
                    .libPaths())

.libPaths(fixed_libpaths)

install.packages('devtools', repo='https://cran.revolutionanalytics.com')
library(devtools)

install_git('https://gitlab.com/lzachmann/ggthemes', force = TRUE)
install_git('https://gitlab.com/apis-staging/greenwave', ref='breadcrumbs')
