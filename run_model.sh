#!/bin/bash
#SBATCH --time=45:00			
#SBATCH --mem=20000                             
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=3

MODEL_R=$1

module load R
module load gcc
module load jags

# This is the "standard" slurm naming for output files. Note also that
# the pattern matching in `roll_up_array_results.R` relies on this format
if [ -n "$SLURM_ARRAY_TASK_ID" ]; then
  OUTPUT_RDS="slurm-${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.rds"
else
  OUTPUT_RDS="slurm-${SLURM_JOB_ID}.rds"
fi

cp ${SLURM_SUBMIT_DIR}/$MODEL_R $PWD
cp ${SLURM_SUBMIT_DIR}/sim_functions.R $PWD

Rscript $(basename "$MODEL_R") $OUTPUT_RDS

cp $OUTPUT_RDS /projects/lci/apis
