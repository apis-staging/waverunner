SCRATCH_DIR=/scratch/$USER
MODEL_R="$1"
SBATCH_ARGS="${@:2}"

ARRAY_JOB_ID=$(sbatch --parsable --workdir=$SCRATCH_DIR SBATCH_ARGS run_model.sh $MODEL_R)

sbatch --depend=after:$ARRAY_JOB_ID roll_up_array_results.sh $ARRAY_JOB_ID $SCRATCH_DIR
