#!/bin/bash
#SBATCH --job-name=gw_test
#SBATCH --workdir=/scratch/jja8
#SBATCH --time=45:00			
#SBATCH --mem=3000                             
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=3

# For an array, either specify option above or just run with e.g
# sbatch --array=1-100%20
# Note I was getting empty output files for some tasks with >20 running at a time
# May be a monsoon limit, but according to monsoon they ran correctly...

# Also note that the workdir will need to be reset for a user. Unfortunately sbatch
# does not seem to allow the use of variables in arguments above, so the best method
# may be to set it at the cl, either with --workdir=/scratch/xyz1 or
# (you can use args on the cl) --workdir=/scratch/$USER

# OR, umm, having a script to run this script?

module load R
module load gcc
module load jags

if [ -n "$SLURM_ARRAY_TASK_ID" ]; then
  OUTPUT_RDS="slurm-${SLURM_ARRAY_JOB_ID}-${SLURM_ARRAY_TASK_ID}.rds"
else
  OUTPUT_RDS="slurm-${SLURM_JOB_ID}.rds"
fi

# Likewise, may not want to hardcode the directory.
cp -r ~/projects/waverunner/src $PWD
Rscript src/harvard.R $OUTPUT_RDS
