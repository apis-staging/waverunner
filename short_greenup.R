library(tools) # file_path_sans_ext
library(greenwave)
source('sim_functions.R')

if (interactive()) {
  output_file <- 'm.rds'
} else {
  output_file <- commandArgs(trailingOnly=TRUE)[1]
}

cache_dir <- file_path_sans_ext(basename(output_file))

# Intercepts (parameter means). The values supplied here assume 365 days per
# year. The necessary adjustments for leap years are made internally.
blank_intercepts <- gw_build_params_array(moment = 'mu', overall = TRUE)

# Temporal effects. Fixed effect units depend on the changepoint parameter.
# For timing (alpha) it's days per unit variance of the covariate with which it
# is associated, while for greenness (gamma) it's change in the VI per unit
# variance. Alternatively, if the standard deviations of each covariate are
# supplied to argument X_k_sd_raw of gw_sim_spacetime(), effects sizes may be
# specified (more intuitively) using the raw/original units of each covariate
# (e.g., days per decade rather than days per sd(decade)).
blank_temporal_fixed_effects <- gw_build_params_array(m = 2, moment = 'mu')

intercepts <- blank_intercepts
intercepts["z", , "timing"] <- c(110, 125, 285, 300)
intercepts["z", , "greenness"] <- c(0.1, 0.6, 0.5, 0.1)

temporal_fixed_effects <- blank_temporal_fixed_effects
temporal_fixed_effects[, "start", "timing"] <- c(0, -5)  #c(3, -1)
temporal_fixed_effects[, "senescense", "timing"] <- c(3, 0)
temporal_fixed_effects["a2", "maturity", "greenness"] <- -.05

temporal_random_effects <- gw_build_params_array(moment = 'sigma')
temporal_random_effects[, , "timing"] <- runif(4, 0, 3)
temporal_random_effects[, , "greenness"] <- runif(4, 0, .025)

model_params <- list(
  use_y="simulated",
  train_fraction=0.6,
  greenness_sd_total=0.05,
  intercepts=intercepts,
  temporal_fixed_effects=temporal_fixed_effects,
  temporal_random_effects=temporal_random_effects,
  cache_dir=cache_dir
)

m <- do.call(run_sim_model, model_params)

saveRDS(m, output_file)
