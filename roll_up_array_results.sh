#!/bin/bash
#SBATCH --time=5:00			
#SBATCH --mem=3000                             
#SBATCH --ntasks=1

module load R
module load gcc

JOB_ID="$1"
WORKDIR="$2"
# Kinda superflous but otherwise we'd probably havta use make or poll squeue...
Rscript roll_up_array_results.R $JOB_ID $WORKDIR
